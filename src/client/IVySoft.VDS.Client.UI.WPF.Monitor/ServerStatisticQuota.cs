﻿using IVySoft.VDS.Client.Api;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace IVySoft.VDS.Client.UI.WPF.Monitor
{
    public class ServerStatisticQuota : INotifyPropertyChanged
    {
        public ServerStatisticQuota(SessionStatisticQuota x)
        {
            this.address_ = x.address;
            this.quota_ = x.quota;
        }

        private string address_;
        private int quota_;
        private long sent_;
        private long sent1m_;
        private int idle_;
        private int delay_;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Address { get => address_; }
        public int Quota
        {
            get => quota_;

            set
            {
                if(this.quota_ != value)
                {
                    this.quota_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Quota)));
                }
            }
        }
        public long Sent
        {
            get => sent_;

            set
            {
                if (this.sent_ != value)
                {
                    this.sent_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Sent)));
                }
            }
        }
        public long Sent1m
        {
            get => sent1m_;

            set
            {
                if (this.sent1m_ != value)
                {
                    this.sent1m_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Sent1m)));
                }
            }
        }
        public int Idle
        {
            get => idle_;

            set
            {
                if (this.idle_ != value)
                {
                    this.idle_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Idle)));
                }
            }
        }
        public int Delay
        {
            get => delay_;

            set
            {
                if (this.delay_ != value)
                {
                    this.delay_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Delay)));
                }
            }
        }

        internal void Update(SessionStatisticQuota y)
        {
            this.Quota = y.quota;
            this.Sent = y.sent;
            this.Sent1m = y.sent1m;
            this.Delay = y.delay;
            this.Idle = y.idle;
        }
    }
}
