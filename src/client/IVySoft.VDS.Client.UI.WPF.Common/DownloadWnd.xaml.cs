﻿using IVySoft.VDS.Client.UI.Logic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IVySoft.VDS.Client.UI.WPF.Common
{
    /// <summary>
    /// Interaction logic for DownloadWnd.xaml
    /// </summary>
    public partial class DownloadWnd : Window
    {
        public new Logic.Model.ChannelMessageFileInfo DataContext
        {
            get
            {
                return (Logic.Model.ChannelMessageFileInfo)base.DataContext;
            }

            set
            {
                this.DataContext = value;
            }
        }

        public DownloadWnd()
        {
            InitializeComponent();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string file_path;
            if (VdsService.DownloadCache.TryGetFile(this.DataContext.Info.Id, out file_path))
            {
                new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo(file_path)
                    {
                        UseShellExecute = true
                    }
                }.Start();
                return;
            }

        }
    }
}
