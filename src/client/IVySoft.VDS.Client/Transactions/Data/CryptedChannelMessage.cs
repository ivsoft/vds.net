﻿namespace IVySoft.VDS.Client.Transactions.Data
{
    internal class CryptedChannelMessage
    {
        public long id { get; set; }
        public string block_id { get; set; }
        public string channel_id { get; set; }
        public string read_id { get; set; }
        public string write_id { get; set; }
        public string crypted_key { get; set; }
        public string crypted_data { get; set; }
        public string signature { get; set; }
    }
}