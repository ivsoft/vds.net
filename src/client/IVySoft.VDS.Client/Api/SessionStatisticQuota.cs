﻿namespace IVySoft.VDS.Client.Api
{
    public class SessionStatisticQuota
    {
        public string address { get; set; }
        public int quota { get; set; }
        public long sent { get; set; }
        public long sent1m { get; set; }
        public int idle { get; set; }
        public int delay { get; set; }
    }
}