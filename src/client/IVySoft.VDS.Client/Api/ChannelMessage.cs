﻿using IVySoft.VDS.Client.Transactions;
using IVySoft.VDS.Client.Transactions.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IVySoft.VDS.Client.Api
{
    public class ChannelMessage
    {
        private readonly long id_;
        private readonly UserMessageTransaction tr_;

        internal ChannelMessage(long id, UserMessageTransaction transaction)
        {
            this.id_ = id;
            this.tr_ = transaction;
        }

        public long Id { get => this.id_; }
        public string Message { get => this.tr_.Message; }
        public IEnumerable<ChannelMessageFileInfo> Files { get => this.tr_.Files.Select(x => new ChannelMessageFileInfo(x)); }
    }
}
