﻿namespace IVySoft.VDS.Client.Api
{
    public class SessionStatistic
    {
        public int send_queue_size { get; set; }
        public int sent_bypes { get; set; }

        public SessionStatisticRow[] items { get; set; }
        public SessionStatisticQuota[] quota { get; set; }
    }
}