﻿using IVySoft.VDS.Client.UI.Logic;
using IVySoft.VDS.Client.UI.WPF.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace IVySoft.VDS.Client.UI.WPF.SmartHome
{
    internal class MainWindowDataContext : INotifyPropertyChanged
    {
        private Api.Channel selected_channel_;
        private DateTime current_date_;
        private double min_time_;
        private double max_time_;
        private DoubleCollection ticks_ = new DoubleCollection();
        private ChannelImage current_file_;
        private readonly List<ChannelImage> files_ = new List<ChannelImage>();
        private bool last_time_mode_ = true;
        private DispatcherTimer update_timer_;
        private HashSet<long> processed_messages_ = new HashSet<long>();

        public ObservableCollection<Api.Channel> ChannelList { get; } = new ObservableCollection<Api.Channel>();

        public Api.Channel SelectedChannel
        {
            get
            {
                return this.selected_channel_;
            }

            set
            {
                if(this.selected_channel_ != value)
                {
                    this.selected_channel_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedChannel)));
                    this.Dates.Clear();
                    this.processed_messages_.Clear();

                    if (null != value)
                    {
                        ProgressWindow.Run(
                            "Get channel's messages",
                            Application.Current.MainWindow,
                            async token =>
                            {
                                using (var s = new VdsService())
                                {
                                    this.files_.Clear();
                                    foreach (var item in await s.Api.GetChannelMessages(token, this.selected_channel_))
                                    {
                                        this.processed_messages_.Add(item.Id);
                                        foreach (var file in item.Files)
                                        {
                                            lock (this.files_)
                                            {
                                                this.files_.Add(new ChannelImage(file));
                                            }
                                        }
                                    }
                                }
                                var dates = new SortedSet<DateTime>();
                                lock (this.files_)
                                {
                                    foreach (var f in this.files_)
                                    {
                                        if (!dates.Contains(f.TimePoint.Date))
                                        {
                                            dates.Add(f.TimePoint.Date);
                                        }
                                    }
                                }

                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    lock (this.files_)
                                    {
                                        this.files_.Sort();
                                    }
                                    foreach (var d in dates)
                                    {
                                        this.Dates.Add(d);
                                    }
                                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Dates)));
                                    if (this.last_time_mode_)
                                    {
                                        this.set_current_date(dates.Max);
                                    }
                                });

                            });
                    }
                }
            }
        }
        
        public ObservableCollection<DateTime> Dates { get; } = new ObservableCollection<DateTime>();
        public DateTime CurrentDate
        {
            get
            {
                return this.current_date_;
            }
            set
            {
                this.last_time_mode_ = false;
                set_current_date(value);
            }
        }

        private void set_current_date(DateTime value)
        {
            var times = new SortedSet<double>();
            lock (this.files_)
            {
                foreach (var f in this.files_)
                {
                    if (f.TimePoint.Date == value)
                    {
                        var tp = f.TimePoint.TimeOfDay.TotalSeconds;
                        if (!times.Contains(tp))
                        {
                            times.Add(tp);
                        }
                    }
                }
            }
            this.min_time_ = times.Min;
            this.max_time_ = times.Max;
            this.ticks_.Clear();
            foreach (var t in times)
            {
                this.ticks_.Add(t);
            }

            if (this.current_date_ != value)
            {
                this.current_date_ = value;

                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentDate)));
            }

            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MinTime)));
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MaxTime)));
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Ticks)));

            if (this.last_time_mode_)
            {
                this.set_current_time(times.Max);
            }
        }

        public double CurrentTime
        {
            get
            {
                return (this.current_file_ == null) ? 0 : this.current_file_.TimePoint.TimeOfDay.TotalSeconds;
            }
            set
            {
                this.last_time_mode_ = false;
                this.set_current_time(value);
            }
        }

        private void set_current_time(double value)
        {
            lock (this.files_)
            {
                foreach (var f in this.files_)
                {
                    if (f.TimePoint.Date == this.current_date_ && value >= f.TimePoint.TimeOfDay.TotalSeconds)
                    {
                        this.CurrentFile = f;
                        this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentTime)));
                        this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentTimeLabel)));
                        return;
                    }
                }
            }
        }

        public string CurrentTimeLabel
        {
            get
            {
                return TimeSpan.FromSeconds(this.CurrentTime).ToString();
            }
        }
        public double MinTime
        {
            get
            {
                return this.min_time_;
            }
        }
        public double MaxTime
        {
            get
            {
                return this.max_time_;
            }
        }

        public DoubleCollection Ticks { get => this.ticks_; }

        public ChannelImage CurrentFile
        {
            get
            {
                return this.current_file_;
            }
            private set
            {
                if(this.current_file_ != value)
                {
                    if(this.current_file_ != null)
                    {
                        this.current_file_.StopDownload();
                    }

                    this.current_file_ = value;
                    this.current_file_.StartDownload();
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentFile)));
                }
            }
        }

        //public ObservableCollection<ChannelImage> Images { get; } = new ObservableCollection<ChannelImage>();
        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowDataContext()
        {
            this.update_timer_ = new DispatcherTimer();
            this.update_timer_.Tick += update_messages;
            this.update_timer_.Interval = TimeSpan.FromSeconds(3);
            this.update_timer_.Start();
        }

        private void update_messages(object sender, EventArgs e)
        {
            var dates = new SortedSet<DateTime>(this.Dates);
            ProgressWindow.Run(
                "Get channel's messages",
                Application.Current.MainWindow,
                async token =>
                {
                    var selected_channel = this.selected_channel_;
                    if (null != selected_channel)
                    {
                        using (var s = new VdsService())
                        {
                            foreach (var item in await s.Api.GetChannelMessages(token, selected_channel, long.MaxValue, 20))
                            {
                                if (!this.processed_messages_.Contains(item.Id))
                                {
                                    this.processed_messages_.Add(item.Id);
                                    foreach (var file in item.Files)
                                    {
                                        lock (this.files_)
                                        {
                                            this.files_.Add(new ChannelImage(file));
                                        }
                                    }
                                }
                            }
                        }
                        var new_dates = new SortedSet<DateTime>();
                        lock (this.files_)
                        {
                            foreach (var f in this.files_)
                            {
                                if (!dates.Contains(f.TimePoint.Date))
                                {
                                    new_dates.Add(f.TimePoint.Date);
                                    dates.Add(f.TimePoint.Date);
                                }
                            }
                        }

                        if (null != Application.Current)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                lock (this.files_)
                                {
                                    this.files_.Sort();
                                }

                                foreach (var d in new_dates)
                                {
                                    this.Dates.Add(d);
                                }
                                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Dates)));
                                if (this.last_time_mode_)
                                {
                                    this.set_current_date(dates.Max);
                                }
                            });
                        }
                    }
                });
        }
    }
}