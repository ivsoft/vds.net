﻿using IVySoft.VDS.Client.Api;
using IVySoft.VDS.Client.UI.Logic;
using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;

namespace IVySoft.VDS.Client.UI.WPF.SmartHome
{
    public class ChannelImage : INotifyPropertyChanged, IComparable<ChannelImage>
    {
        private string image_url_;
        private string image_label_;
        private ChannelMessageFileInfo item_;
        private static Regex name_parser_ = new Regex(@"(\d+)-(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\.*)", RegexOptions.Compiled);
        private int download_progress_;
        private CancellationTokenSource token_;

        public ChannelImage(ChannelMessageFileInfo item)
        {
            this.item_ = item;
            this.image_label_ = item.Name;
            var m = name_parser_.Match(item.Name);
            if (m.Success)
            {
                this.TimePoint = new DateTime(int.Parse(m.Groups[2].Value), int.Parse(m.Groups[3].Value), int.Parse(m.Groups[4].Value),
                    int.Parse(m.Groups[5].Value), int.Parse(m.Groups[6].Value), int.Parse(m.Groups[7].Value), DateTimeKind.Utc);
            }
        }

        public DateTime TimePoint { get; set; }

        public ChannelMessageFileInfo Item { get => this.item_; }

        public void StartDownload()
        {
            string cached;
            if (VdsService.DownloadCache.TryGetFile(this.item_.Id, out cached))
            {
                this.Progress = 100;
                this.ImageUrl = cached;
            }
            else
            {
                lock (this)
                {
                    if (this.token_ == null)
                    {
                        this.token_ = new CancellationTokenSource(TimeSpan.FromSeconds(600));
                        ThreadPool.QueueUserWorkItem(new WaitCallback(this.download_file));
                    }
                }
            }
        }

        public void StopDownload()
        {
            lock (this)
            {
                if (this.token_ != null)
                {
                    this.token_.Cancel();
                    this.token_ = null;
                }
            }
        }

        private void download_file(object state)
        {
            try
            {
                using (var s = new VdsService())
                {
                    var f = s.Download(
                        this.token_.Token,
                        this.item_,
                        progress =>
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                                                {
                                                    this.Progress = progress;
                                                });
                            return true;
                        }).Result;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        this.ImageUrl = f;
                    });
                }
            }
            catch (Exception ex)
            {
                lock (this)
                {
                    if (this.token_ != null)
                    {
                        this.token_.Cancel();
                        this.token_ = null;
                    }
                }

                var message = UIUtils.GetErrorMessage(ex);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.ImageLabel += "(" + message + ")";
                });
            }
        }

        public int CompareTo([AllowNull] ChannelImage other)
        {
            return other.TimePoint.CompareTo(this.TimePoint);
        }

        public string ImageUrl
        {
            get => image_url_;
            set
            {
                if (this.image_url_ != value)
                {
                    this.image_url_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImageUrl)));
                }
            }
        }
        public string ImageLabel
        {
            get => image_label_;
            set
            {
                if (this.image_label_ != value)
                {
                    this.image_label_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImageLabel)));
                }
            }
        }
        public int Progress
        {
            get => download_progress_;
            set
            {
                if (this.download_progress_ != value)
                {
                    this.download_progress_ = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Progress)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}