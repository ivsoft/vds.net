#!/bin/bash
set -e
set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
src_path=/home/user/.vs/vds/a50087f0-64a6-4da3-aa76-2432d1f15b43/out/build/WSL-Clang-Debug/

cp ${src_path}app/vds_background/vds_background bin/Debug/netcoreapp3.1/
cp ${src_path}app/vds_ws_server/vds_ws_server bin/Debug/netcoreapp3.1/
cp ${src_path}../../../src/keys bin/Debug/netcoreapp3.1/
dotnet test