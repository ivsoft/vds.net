﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace IVySoft.VDS.Client.Cmd.Tests
{
    public class VDSTest
    {
        private readonly ITestOutputHelper output_;

        public VDSTest(ITestOutputHelper output)
        {
            this.output_ = output;
        }

        protected void WriteLine(string line)
        {
            this.output_.WriteLine(line);
            Debug.WriteLine("Test: " + line);
        }

        protected void CompareFile(string source_file, string target_file)
        {
            var buffer1 = new byte[1024];
            var buffer2 = new byte[1024];

            using (var f1 = File.OpenRead(source_file))
            {
                using (var f2 = File.OpenRead(target_file))
                {
                    for (; ; )
                    {
                        var readed1 = f1.Read(buffer1, 0, buffer1.Length);
                        var readed2 = f2.Read(buffer2, 0, buffer2.Length);

                        Assert.Equal(readed1, readed2);

                        if (readed1 == 0)
                        {
                            break;
                        }

                        Assert.True(buffer1.SequenceEqual(buffer2));
                    }
                }
            }
        }

        protected void GenerateRandomFile(Random rnd, string filePath)
        {
            var size = rnd.Next();
            while (size < 1 || size > 10000)
            {
                size = rnd.Next();
            }

            using (var f = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                while (0 < size)
                {
                    f.WriteByte((byte)rnd.Next());
                    --size;
                }

                f.Flush();
            }
        }

    }
}